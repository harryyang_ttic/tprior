function [ H, h ] = AssignH2( W, sigma, top_k )
%ASSIGNH2 Summary of this function goes here
%   Detailed explanation goes here
[H,~]=size(W);
n=H;
C=zeros(n,n,n);
for i=1:n
    for j=i+1:n
        for k=j+1:n
            C(i,j,k)=sqrt(W(i,j)*W(i,j)+W(j,k)*W(j,k)+W(i,k)*W(i,k))+sigma(i,j,k);
        end
    end
end

[sorted_vals,sorted_idx] = sort(C(:),'ascend'); 
c = cell([1 numel(size(C))]); 
[c{:}] = ind2sub(size(C),sorted_idx);
out = [num2cell(sorted_vals) mat2cell([c{:}],ones(1,numel(C)),numel(size(C)))]; 

H=zeros(3,top_k);
h=zeros(1,top_k);
for i=1:top_k
    tmp=out(i,2);
    id=tmp{1};
    H(:,i)=id';
    h(i)=1/log(i+1);
end

end

