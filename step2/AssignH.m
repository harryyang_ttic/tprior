function [ H,h ] = AssignH( W, top_k )
%ASSIGNH Summary of this function goes here
%   Detailed explanation goes here

absW=abs(W);
[H,~]=size(W);
n=H;
C=zeros(n,n,n);
for i=1:n
    for j=i+1:n
        for k=j+1:n
            C1=absW(i,j)+absW(j,k)-absW(i,k);
            C2=absW(j,k)+absW(i,k)-absW(i,j);
            C3=absW(i,k)+absW(i,j)-absW(k,j);
            C(i,j,k)=max(max(C1,C2),C3);
        end
    end
end

[sorted_vals,sorted_idx] = sort(C(:),'descend'); 
c = cell([1 numel(size(C))]); 
[c{:}] = ind2sub(size(C),sorted_idx);
out = [num2cell(sorted_vals) mat2cell([c{:}],ones(1,numel(C)),numel(size(C)))]; 

H=zeros(3,top_k);
h=zeros(1,top_k);
for i=1:top_k
    tmp=out(i,2);
    id=tmp{1};
    H(:,i)=id';
    h(i)=1/log(i+1);
end

end

