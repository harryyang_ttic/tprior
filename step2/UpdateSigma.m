function [ sigma ] = UpdateSigma( sigma, H1,h1,H2,h2,delta,top_k )
%UPDATESIGMA Summary of this function goes here
%   Detailed explanation goes here
for i=1:top_k
    sigma(H1(1,i),H1(2,i),H1(3,i))=sigma(H1(1,i),H1(2,i),H1(3,i))+delta*h1(i);
    sigma(H2(1,i),H2(2,i),H2(3,i))=sigma(H2(1,i),H2(2,i),H2(3,i))-delta*h2(i);
end

end

