function [ Y ] = TestIter2( X,U,H,h,beta,alpha,rho)
%TESTITER2 Summary of this function goes here
%   Detailed explanation goes here
[~,N]=size(X);
t=N*(N-1)/2;
X2=zeros(t,1);
U2=zeros(t,1);
for i=1:N
    for j=i+1:N
        tmp=Convert(i,j,N);
        X2(tmp,1)=X(i,j);
        U2(tmp,1)=U(i,j);
    end
end

[~,top_k]=size(H);
H2=zeros(3,top_k);
for i=1:top_k
    H2(1,i)=Convert(H(1,i),H(2,i),N);
    H2(2,i)=Convert(H(2,i),H(3,i),N);
    H2(3,i)=Convert(H(1,i),H(3,i),N);
end
X_out=iter2(X2,U2,beta,alpha,rho,H2,h);
Y=zeros(N,N);

for i=1:N
    for j=1:N
        if(j<i)
            tmp=Convert(j,i,N);
        elseif (j>i)
            tmp=Convert(i,j,N);
        end
        Y(i,jzeros(n,n,n))=X_out(tmp);
    end
end

%% compute diagonal value

for i=1:N
    x=X(i,i);
    u=U(i,i);
    tmp=-(alpha-beta*(x+u))/beta;
    if(tmp<0)
        minVal=beta*(x+u)^2/2;
        y=0;
    else
        minVal=beta*(x-tmp+u)^2/2+alpha*tmp;
        y=tmp;
    end
    tmp=(beta*(x+u)+alpha)/beta;
    if(tmp>=0)
        minVal2=beta*(x+u)^2/2;
        y2=0;
    else
        minVal2=beta*(x-tmp+u)^2/2-alpha*tmp;
        y2=tmp;
    end
    if(minVal<minVal2)
        Y(i,i)=y;
    else
        Y(i,i)=y2;
    end
end

end

