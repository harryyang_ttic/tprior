function [ X_out ] = iter2(X,U,beta,alpha,rho,H,h)
%ITER2 Summary of this function goes here
%   Detailed explanation goes here
Y=-(X+U);
[height,~]=size(Y);
A=eye(height,height);
z=zeros(1,2);
z(1)=alpha/beta;
z(2)=rho/beta;

opts=[];
opts.init=2;       
opts.tFlag=3;      
opts.maxIter=5000;  % maximum number of iterations
opts.tol=1e-5;      % the tolerance parameter
% regularization
opts.rFlag=1;       % use ratio
% Normalization
opts.nFlag=0;       % without normalization
[~,top_k]=size(H);
G=reshape(H',1,3*top_k);
W=zeros(3,top_k);
for i=1:top_k
    W(1,i)=3*i-2;
    W(2,i)=3*i;
    W(3,i)=h(i);
end
opts.G=G;
opts.ind=W;
opts.rStartNum=100;
opts.maxIter2=1000;
opts.tol2=1e-8;
opts.flag2=2;

[X_out, ~, ~]= overlapping_LeastR(A, Y, z, opts);

end

