function [ X ] = iter1( S, beta, Y, U )

[H,W]=size(S);
assert(H==W);
beta=0.1;
Y=S;
A=(S+beta*U-beta*Y)/beta;
I=eye(H,H);
B=-I/beta;
K=-B+A*A/4;
[U,S1,V]=svd(K);
S1_root=S1^0.5;
X=V*S1_root*V'-A/2;

end

