clear;

%% data initialization
S=textread('Test_Covariance');
beta=0.1;
Y=S;
U=zeros(size(S));

%% run first step
cd step1
X=iter1(S,beta,Y,U);
cd ..

%% initialize permutation with glasso 
% H is 3*5000 matrix where H(1,i), H(2,i), H(3,i) has cost h(i) in
% descending order
cd glassoInit
top_k=5000;
[H,h,alpha]=InitiSolver(S,0.05,top_k);
cd ..

%% add path to overlapping group lasso
root=cd;
addpath(genpath([root '/SLEP']));

%% solve overlapping group lasso given H and h
iter=2;
[n,~]=size(S);
sigma=zeros(n,n,n);
delta=0.05;
cd step2
for i=1:iter
    rho=1;
    Y = TestIter2(X,U,H,h,beta,alpha,rho);
    [H,h]=AssignH(Y,top_k);
    [H2, h2]=AssignH2(Y,sigma, top_k);
    sigma=UpdateSigma(sigma,H,h,H2,h2, delta, top_k);
    H=H2;
end
cd ..
%% solve step 3
U=U+X-Y;